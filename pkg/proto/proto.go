package proto

const (
	ServiceALL  = "*"
	ServiceISLB = "islb"
	ServiceROOM = "room"
	ServiceRTC  = "rtc"
	ServiceSIG  = "signal"
)
