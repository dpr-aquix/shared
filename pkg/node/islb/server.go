package islb

import (
	"gitlab.com/dpr-aquix/shared/pkg/db"
	islb "gitlab.com/dpr-aquix/shared/proto/islb"
)

type islbServer struct {
	islb.UnimplementedISLBServer
	redis *db.Redis
	islb  *ISLB
	conf  Config
}

func newISLBServer(conf Config, in *ISLB, redis *db.Redis) *islbServer {
	return &islbServer{
		conf:  conf,
		islb:  in,
		redis: redis,
	}
}
